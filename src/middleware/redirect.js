export default function ({ route, redirect, store }) {
  // If the user is not authenticated
  if (route.fullPath === '/' || route.fullPath === '' || route.fullPath === '/detail') {
    return redirect('/dashboard')
  } else {
    switch (route.fullPath) {
      case '/dashboard':
        store.dispatch('setMessages', 'fetched dashboard')
        break
      case '/heroes':
        store.dispatch('setMessages', 'fetched heroes')
        break
      case `/detail/${route.params.id}`:
        store.dispatch('setMessages', `fetched hero id=${route.params.id}`)
        break
    }
  }
}
