export default {
  addMessages(state, message) {
    state.messages.push(message)
  },
  clearMessages(state) {
    state.messages.splice(0)
  },
  addHeroes(state, heroes) {
    delete heroes.type
    state.heroes.push(heroes)
  },
  changeHeroes(state, heroes) {
    delete heroes.type
    const findId = state.heroes.findIndex(e => e.id === heroes.id)
    state.heroes[findId].name = heroes.name
  },
  clearHeroes(state, heroes) {
    const findId = state.heroes.findIndex(e => e.id === heroes.id)
    state.heroes.splice(findId, 1)
  },
}
