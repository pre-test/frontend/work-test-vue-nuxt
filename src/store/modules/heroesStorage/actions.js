export default {
  setMessages({ commit }, message) {
    message ? commit('addMessages', message) : commit('clearMessages')
  },
  setHeroes({ commit }, heroes) {
    switch (heroes.type) {
      case 'add': commit('addHeroes', heroes)
        break
      case 'change': commit('changeHeroes', heroes)
        break
      case 'delete': commit('clearHeroes', heroes)
        break
    }
  }
}
