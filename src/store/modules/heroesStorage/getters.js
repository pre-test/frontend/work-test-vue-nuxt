export default {
  'heroes/getMessage': state => state.messages,
  'heroes/getHeroes': state => state.heroes
}
