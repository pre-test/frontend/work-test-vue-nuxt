import Vuex from 'vuex'
import heroesModule from './modules/heroesStorage'

export default () => new Vuex.Store({
  stateFactory: true,
  namespaced: true,
  modules: {
    heroesStorage: heroesModule
  }
})
